#define ROZDZIELCZOSC 10

void pomaluj_pole(int plansza[80][60],int x, int y, int kolor){
    setcolor(kolor);
    rectangle(x*10,y*10,(x+1)*10,(y+1)*10);
    setfillstyle(SOLID_FILL,kolor);
    floodfill(x*ROZDZIELCZOSC+2,y*ROZDZIELCZOSC+2,kolor);
    plansza[x][y]=kolor;
}

char przycisk (void){                           //Zwraca kod wcisnietego przycisku
    char klawisz;
        klawisz=getch();
        if(klawisz==0 || klawisz==224){
            klawisz=przycisk();
            if(klawisz==75)
                klawisz=97;
            if(klawisz==77)
                klawisz=100;
            if(klawisz==80)
                klawisz=115;
            if(klawisz==72)
                klawisz=119;
            }
  //  printf("Klawisz %c ma kod %d\n\n", klawisz, klawisz); //sprawdzenie kodu klawisza
    return klawisz;
}

void zapisz_do_pliku(int wynik){
	FILE *statystyka;

	statystyka = fopen("statystyka.txt", "w");

	fprintf(statystyka, "%d", wynik);
	fclose(statystyka);

}

double wczytaj(){
	FILE *statystyka;

	double statystyka_odczytana = 0;

	statystyka = fopen("statystyka.txt", "r");

	if (statystyka != NULL)

		fscanf(statystyka, "%lf", &statystyka_odczytana);

	fclose(statystyka);
	return statystyka_odczytana;
}
