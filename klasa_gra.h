#define ROZDZIELCZOSC 10
#define TAK 1
#define NIE 0
#define WYGRANA 666

class gra{
    public:
    int polegry[80][60];
    char klaw,klawpoprz;
    char *wyn_wys;
    int poziom, wynik, rekord;
    int do_zamkniecia;
    wspolrzedna rog,wsgracza;
    opiskuli kula[3];
    void inicjalizuj();
    void przygotuj();
    void wybor_poziomu();
    char odswiez();
    void sterow();
    opiskuli kulka(opiskuli kula);
    void zamknij();
    void konwersja();
    void pauza();
};

void gra::inicjalizuj(){
    initwindow(800,625);
    char *wyn_wys = (char*) malloc(2*sizeof(char));
}

void gra::przygotuj(){

    rog.X=0;
    rog.Y=0;
    wsgracza.X=0;
    wsgracza.Y=0;


    srand( time( NULL ) );


    kula[1].X=rand()%79+1;
    kula[1].Y=rand()%59+1;
    kula[1].DX=1;
    kula[1].DY=1;

    kula[2].X=rand()%79+1;
    kula[2].Y=rand()%59+1;
    kula[2].DX=-1;
    kula[2].DY=1;

    kula[3].X=rand()%79+1;
    kula[3].Y=rand()%59+1;
    kula[3].DX=1;
    kula[3].DY=-1;

    for(int i=0;i<80;i++)
        for(int j=0;j<60;j++)
            polegry[i][j]=0;

    setfillstyle(SOLID_FILL,BLACK);
    floodfill(2,2,BLUE);


    for(int i=0;i<80;i++)
        pomaluj_pole(polegry,i,0,RED);
    for(int i=0;i<80;i++)
        pomaluj_pole(polegry,i,59,RED);
    for(int i=0;i<59;i++)
        pomaluj_pole(polegry,0,i,RED);
    for(int i=0;i<59;i++)
        pomaluj_pole(polegry,79,i,RED);

    setcolor(BLUE);
    rectangle(0,601,800,625);
    setfillstyle(SOLID_FILL,BLACK);
    floodfill(1,602,BLUE);
    setcolor(BLACK);
    rectangle(0,601,800,625);
    setcolor(YELLOW);

    outtextxy(140,605, "--");
    outtextxy(160,605," % zaslonietej planszy                Poziom:");
    outtextxy(660,605,"  Wyjscie: ESC");

    klaw=97;
    klawpoprz=97;
    do_zamkniecia=NIE;
}

void gra::wybor_poziomu (){
    setcolor(YELLOW);
    outtextxy(305,290,"Wybierz poziom trudnosci: 1-3");

    do{

    if(kbhit())
        klaw=przycisk();

    }while(klaw!=49&&klaw!=50&&klaw!=51);

    if(klaw==49){
        poziom=1;
        outtextxy(435,605,"1");}
    if(klaw==50){
        poziom=2;
        outtextxy(435,605,"2");}
    if(klaw==51){
        poziom=3;
        outtextxy(435,605,"3");}

    setcolor(BLACK);
    outtextxy(305,290,"Wybierz poziom trudnosci: 1-3");

}

char gra::odswiez(){
        if(kbhit()){
                if(getpixel(wsgracza.X*ROZDZIELCZOSC+5,wsgracza.Y*ROZDZIELCZOSC+5)!=RED)
                    klawpoprz=klaw;
            klaw=przycisk();
            if(klaw!=97&&klaw!=100&&klaw!=115&&klaw!=119&&klaw!=27)
                klaw=klawpoprz;
        }
        delay(50);
        if(klaw==27)
            pauza();
        sterow();
        if(do_zamkniecia==TAK)
            zamknij();
        if(wynik==WYGRANA)
            klaw=27;
        kula[1]=kulka(kula[1]);
        if(GREEN==polegry[kula[1].X][kula[1].Y])
            klaw=27;
        if(poziom>1){
            kula[2]=kulka(kula[2]);
            if(GREEN==polegry[kula[2].X][kula[2].Y])
                klaw=27;
        }
        if(poziom>2){
            kula[3]=kulka(kula[3]);
            if(GREEN==polegry[kula[3].X][kula[3].Y])
                klaw=27;
        }

        return klaw;
}

void gra::pauza(){

        setcolor(YELLOW);
        outtextxy(375,190,"PAUZA");
        outtextxy(200,290,"Wcisnij cokolwiek by kontynuowac, lub ESC by zakonczyc program.");

        if(przycisk()!=27)
            klaw=klawpoprz;
        else
            exit(0);

        setcolor(BLACK);
        outtextxy(375,190,"PAUZA");
        outtextxy(200,290,"Wcisnij cokolwiek by kontynuowac, lub ESC by zakonczyc program.");

}

void gra::konwersja(){
    switch(wynik/240){
        case 1:
            wyn_wys="05";
        break;
        case 2:
            wyn_wys="10";
        break;
        case 3:
            wyn_wys="15";
        break;
        case 4:
            wyn_wys="20";
        break;
        case 5:
            wyn_wys="25";
        break;
        case 6:
            wyn_wys="30";
        break;
        case 7:
            wyn_wys="35";
        break;
        case 8:
            wyn_wys="40";
        break;
        case 9:
            wyn_wys="45";
        break;
        case 10:
            wyn_wys="50";
        break;
        case 11:
            wyn_wys="55";
        break;
        case 12:
            wyn_wys="60";
        break;
       case 13:
            wyn_wys="65";
        break;
        case 14:
            wyn_wys="70";
        break;
        case 15:
            wyn_wys="75";
        break;
        case 16:
            wyn_wys="80";
        break;
        case 17:
            wyn_wys="85";
        break;
        case 18:
            wyn_wys="90";
        break;
        case 19:
            wyn_wys="95";
        break;
        case 0:
            wyn_wys="00";
        break;
    }
}


void gra::sterow (){
        if(getpixel(wsgracza.X*ROZDZIELCZOSC+5,wsgracza.Y*ROZDZIELCZOSC+5)==RED){
            pomaluj_pole(polegry,wsgracza.X,wsgracza.Y,RED);
            do_zamkniecia=NIE;
        }
        if(getpixel(wsgracza.X*ROZDZIELCZOSC+5,wsgracza.Y*ROZDZIELCZOSC+5)==GREEN){
            if(klawpoprz==97&&klaw==100)
                klaw=97;
            if(klawpoprz==100&&klaw==97)
                klaw=100;
            if(klawpoprz==119&&klaw==115)
                klaw=119;
            if(klawpoprz==115&&klaw==119)
                klaw=115;
        }
        if(klaw==97)
            if(wsgracza.X-1>=rog.X){
                wsgracza.X=wsgracza.X-1;
            }
        if(klaw==119)
            if(wsgracza.Y-1>=rog.Y){
                wsgracza.Y=wsgracza.Y-1;
            }
        if(klaw==115)
            if(wsgracza.Y+1<=rog.Y+59){
                wsgracza.Y=wsgracza.Y+1;
            }
        if(klaw==100)
            if(wsgracza.X+1<=rog.X+79){
                wsgracza.X=wsgracza.X+1;
            }
        if(getpixel(wsgracza.X*ROZDZIELCZOSC+5,wsgracza.Y*ROZDZIELCZOSC+5)==RED){
            setcolor(DARKGRAY);
            rectangle(wsgracza.X*ROZDZIELCZOSC+2,wsgracza.Y*ROZDZIELCZOSC+2,wsgracza.X*ROZDZIELCZOSC+ROZDZIELCZOSC-2,wsgracza.Y*ROZDZIELCZOSC+ROZDZIELCZOSC-2);
            }
        if(getpixel(wsgracza.X*ROZDZIELCZOSC+5,wsgracza.Y*ROZDZIELCZOSC+5)==GREEN)
            klaw=27;
        if(getpixel(wsgracza.X*ROZDZIELCZOSC+5,wsgracza.Y*ROZDZIELCZOSC+5)==BLACK){
            pomaluj_pole(polegry,wsgracza.X,wsgracza.Y,GREEN);
            do_zamkniecia=TAK;
        }

}

opiskuli gra::kulka (opiskuli kula){
        setcolor(BLACK);
        circle(kula.X*ROZDZIELCZOSC+5,kula.Y*ROZDZIELCZOSC+5,4);
        setcolor(YELLOW);

        if(RED==polegry[kula.X+1*kula.DX][kula.Y])
            kula.DX=kula.DX*(-1);

        if(RED==polegry[kula.X][kula.Y+1*kula.DY])
            kula.DY=kula.DY*(-1);

        kula.X=kula.X+1*kula.DX;
        kula.Y=kula.Y+1*kula.DY;
        circle(kula.X*ROZDZIELCZOSC+5,kula.Y*ROZDZIELCZOSC+5,4);
        return kula;

}


void gra::zamknij(){
      wynik=0;
    do_zamkniecia=TAK;
   if(getpixel(wsgracza.X*ROZDZIELCZOSC+5,wsgracza.Y*ROZDZIELCZOSC+5)==RED){
            for(int i=0;i<80;i++)
                for(int j=0;j<60;j++)
                    if(polegry[i][j]==GREEN)
                        pomaluj_pole(polegry,i,j,RED);

    setfillstyle(SOLID_FILL,COLOR(4,4,4));
    floodfill(kula[1].X*ROZDZIELCZOSC+5,kula[1].Y*ROZDZIELCZOSC+5,RED);
    if(poziom>1){
        floodfill(kula[2].X*ROZDZIELCZOSC+5,kula[2].Y*ROZDZIELCZOSC+5,RED);
        }
    if(poziom>2){
        floodfill(kula[3].X*ROZDZIELCZOSC+5,kula[3].Y*ROZDZIELCZOSC+5,RED);
        }

    int i=15;
    int j=15;


        do{
            do{
                if(getpixel(i,j)==BLACK){
                    setfillstyle(SOLID_FILL,RED);
                    floodfill(i,j,COLOR(4,4,4));
                    do_zamkniecia=NIE;
                }
                i=i+ROZDZIELCZOSC;
            }while(do_zamkniecia!=NIE&&i<800);
            j=j+ROZDZIELCZOSC;
            i=15;
        }while(do_zamkniecia!=NIE&&j<600);

    setfillstyle(SOLID_FILL,BLACK);
    floodfill(kula[1].X*ROZDZIELCZOSC+5,kula[1].Y*ROZDZIELCZOSC+5,RED);
    if(poziom>1){
        floodfill(kula[2].X*ROZDZIELCZOSC+5,kula[2].Y*ROZDZIELCZOSC+5,RED);
        }
    if(poziom>2){
        floodfill(kula[3].X*ROZDZIELCZOSC+5,kula[3].Y*ROZDZIELCZOSC+5,RED);
        }

    for(int i=0;i<80;i++)
        for(int j=0;j<60;j++)
            if(getpixel(i*ROZDZIELCZOSC+5,j*ROZDZIELCZOSC+5)==RED)
                wynik++;


    konwersja();
    if(poziom==3)
        if(wynik>wczytaj()){
            zapisz_do_pliku(wynik);
            rekord=TAK;
        }
        else
            rekord=NIE;


    if(wynik>3669)
            wynik=WYGRANA;

    setcolor(BLUE);
    rectangle(0,601,800,625);
    setfillstyle(SOLID_FILL,BLACK);
    floodfill(1,602,BLUE);
    setcolor(BLACK);
    rectangle(0,601,800,625);
    setcolor(YELLOW);


    outtextxy(140,605, wyn_wys);
    outtextxy(160,605," % zaslonietej planszy                Poziom:");
    outtextxy(660,605,"  Wyjscie: ESC");
    switch(poziom){
        case 1:
            outtextxy(435,605,"1");
        break;
       case 2:
            outtextxy(435,605,"2");
        break;
        case 3:
            outtextxy(435,605,"3");
        break;
    }

    klaw=0;
    klawpoprz=0;

    }
}

